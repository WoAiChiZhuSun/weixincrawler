### 基于 Python 实现微信公众号爬虫
### by Clark

## 分析：
我们直接从 Fiddler 请求中拷贝 URL 和 Headers， 右键 -> Copy -> Just Url/Headers Only
### url分析
```python
url = https://mp.weixin.qq.com/mp/profile_ext
    ?action=home
    &__biz=MjM5MzgyODQxMQ==
    &scene=124
    &devicetype=android-24
    &version=26060132
    &lang=zh_CN
    &nettype=WIFI
    &a8scene=3
    &pass_ticket=Bb1Ls%2BE9nT3lNOctnEBdSwEbyqd0OE60stKUEUaAKC0%3D
    &wx_header=1
```

### heaers
```python
# 请求行：
GET https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MjM5MzgyODQxMQ==&scene=124&devicetype=android-24&version=26060132&lang=zh_CN&nettype=WIFI&a8scene=3&pass_ticket=Bb1Ls%2BE9nT3lNOctnEBdSwEbyqd0OE60stKUEUaAKC0%3D&wx_header=1 HTTP/1.1
# 请求体：
Host: mp.weixin.qq.com
Connection: keep-alive
User-Agent: Mozilla/5.0 (Linux; Android 7.0; EVA-AL00 Build/HUAWEIEVA-AL00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.132 MQQBrowser/6.2 TBS/043806 Mobile Safari/537.36 MicroMessenger/6.6.1.1200(0x26060132) NetType/WIFI Language/zh_CN
x-wechat-key: c84b833784e89b00effc845e799d0f0a5d70cb51b660a20471de7f18d155e49d64721d143c36777099cc7237f9f3986b1d5d4ca930ee0d80ae7a76b9e09759f36a7b6d70a2f3f9d9af2fb270c5a4da1f
x-wechat-uin: MjcwMTU4MTU%3D
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,image/wxpic,image/sharpp,image/apng,*/*;q=0.8
Accept-Encoding: gzip, deflate
Accept-Language: zh-CN,en-GG;q=0.8,en-US;q=0.6
Cookie: pgv_pvi=1374336000; pgv_si=s6228196352; wxtokenkey=79772e8e485a4383e4e75ae78c8f25d8cf68789c47dca6c98fcafd20ec767fa3; rewardsn=19b5fd2447aa202e2759; wxuin=0; devicetype=android-24; version=26060132; lang=zh_CN; pass_ticket=Bb1Ls+E9nT3lNOctnEBdSwEbyqd0OE60stKUEUaAKC0=; wap_sid2=CIf18AwSiAFxOFhHR3FUMnA5bmU5elc2UlNDVFFVaG1WdktIZ0JDVHhEbDh1TFlnc0xvR2Jtd1Azc0dtbXNMYktPTzdTNmVJc25LVFRGZ3ZLSGVnSmxDS3ZuTy1SWC15aDRLeU5Fb3dZaEx4eFFtRkd6Z2pCU1R5ODhoVXFfNHR3ZjhKcmxSV3JRTUFBQX5+MPTJp9MFOA1AlU4=
Q-UA2: QV=3&PL=ADR&PR=WX&PP=com.tencent.mm&PPVN=6.6.1&TBSVC=43602&CO=BK&COVC=043806&PB=GE&VE=GA&DE=PHONE&CHID=0&LCID=9422&MO= EVA-AL00 &RL=1080*1794&OS=7.0&API=24
Q-GUID: 3c75d4e0dfdbf7bc88e6faf213b788cb
Q-Auth: 31045b957cf33acf31e40be2f3e71c5217597676a9729f1b

# 响应行：
HTTP/1.1 200 OK
# 响应头：
RetKey: 14
LogicRet: 0
Content-Type: text/html; charset=UTF-8
Cache-Control: no-cache, must-revalidate
Set-Cookie: wxuin=0; Path=/; HttpOnly
Set-Cookie: devicetype=android-24; Path=/
Set-Cookie: version=26060132; Path=/
Set-Cookie: lang=zh_CN; Path=/
Set-Cookie: pass_ticket=Bb1Ls+E9nT3lNOctnEBdSwEbyqd0OE60stKUEUaAKC0=; Path=/; HttpOnly
Set-Cookie: wap_sid2=CIf18AwSiAFxOFhHR3FUMnA5bmU5elc2UlNDVFFVaG1WdktIZ0JDVHhEbDh1TFlnc0xvR2Jtd1Azc0dtbXNMYktPTzdTNmVJc25LVFRGZ3ZLSGVnSmxDS3ZuTy1SWC15aDRLeU5Fb3dZaEx4eFFtRkd6Z2pCU1R5ODhoVXFfNHR3ZjhKcmxSV3JRTUFBQX5+MPTJp9MFOAxAlE4=; Path=/; HttpOnly
Connection: keep-alive
Content-Length: 32474
```

### html 转义字符处理
Python 反转义字符串
用 Python 来处理转义字符串有多种方式，而且 py2 和 py3 中处理方式不一样，在 python2 中，反转义串的模块是 HTMLParser。
```python
# python2
import HTMLParser
>>> HTMLParser().unescape('a=1&amp;b=2')
'a=1&b=2'
Python3 HTMLParser 模块迁移到了 html.parser

# python3
>>> from html.parser import HTMLParser
>>> HTMLParser().unescape('a=1&amp;b=2')
'a=1&b=2'
到 python3.4 以后的版本，在 html 模块新增了 unescape 方法。

# python3.4
>>> import html
>>> html.unescape('a=1&amp;b=2')
'a=1&b=2'
推荐最后一种写法，因为 HTMLParser.unescape 方法在 Python3.4 就已经被废弃掉不推荐使用了，意味着之后的版本会被彻底移除。

另外，sax 模块也有支持反转义的函数

>>> from xml.sax.saxutils import unescape
>>> unescape('a=1&amp;b=2')
'a=1&b=2'
当然，你完全可以实现自己的反转义功能，也不复杂，当然，我们崇尚不重复造轮子。
```

### 难点
#### html转义字符串进行反转义unescape
![](https://ws3.sinaimg.cn/large/006tNc79ly1fntcb7qdhuj31kw0vemyi.jpg)
#### 正则表达式
#### 分析
1.从反转义html里 提取的到的json内容；
2.最终得到一个列表对象，返回最近发布的10篇文章；
3.分析每条数据返回有哪些字段。
![](https://ws2.sinaimg.cn/large/006tNc79ly1fntcseybkcj31kw0vf79e.jpg)
![](https://ws4.sinaimg.cn/large/006tNc79ly1fntclz01xjj31kw0v6ag5.jpg)


上面这张图是微信作为单次推送发给用户的多图文消息，有发送时间对应comm_msg_info.datetime，app_msg_ext_info中的字段信息就是第一篇文章的字段信息，分别对应：

title：文章标题
content_url：文章链接
source_url：原文链接，有可能为空
digest：摘要
cover：封面图
datetime：推送时间
后面几篇文章以列表的形式保存在 multi_app_msg_item_list 字段中。

#### 运行结果
总共10个结果，但是代码里有2个遍历输出print（tem），所以输出20个
![](https://ws2.sinaimg.cn/large/006tNc79ly1fntd271z2qj31kw0zjjxd.jpg)
